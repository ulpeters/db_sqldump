## sqldump

Dumps all DBs to ./data and then sleeps for the next 24h.

### Setup
1. Set variables DB name and DB root password in `.env`
```
echo -e \
'DB_HOST=db'\
'\n'\
'MYSQL_ROOT_PASSWORD=PutYourPasswordHere'\
> .env
```
