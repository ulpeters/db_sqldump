#!/bin/bash -xe
# Daily dump all DBs in individual files

while true; do
  echo "Start backup at "`date`
  # Get a list of all DBs to iterate in the next steps
  databases=`mysql --host=$DB_HOST --user=root --password=$MYSQL_ROOT_PASSWORD --batch --disable-column-names --execute "show databases;"`

  for db in $databases;do
    # Skip internal databases which cannot be dumped
    if [ $db = "information_schema" ]; then echo "--> skip $db"; continue; fi
    if [ $db = "performance_schema" ]; then echo "--> skip $db"; continue; fi
    echo "--> dump $db"
    mysqldump --host=$DB_HOST --user=root --password=$MYSQL_ROOT_PASSWORD --databases $db > /mnt/backup/$db.sql
  done

  # Dump users, passwords and grants
  mysqldump --host=$DB_HOST --user=root --password=$MYSQL_ROOT_PASSWORD --system=user --insert-ignore > /mnt/backup/grants.sql

  echo "Sleeping for 1d ..................tzzzzz"
  sleep 86400
done
